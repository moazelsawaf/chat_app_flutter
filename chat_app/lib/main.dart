import 'package:flutter/material.dart';
import 'package:flutter_day_project/screens/chat_screen.dart';
import 'package:flutter_day_project/screens/login_screen.dart';
import 'package:flutter_day_project/util/constants.dart';

import 'screens/signup_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: kYellow,
          accentColor: kBrown,
          buttonColor: kBrown,
          hintColor: kLightBrown,
          cursorColor: kYellow),
      routes: {
        '/login': (context) => LoginScreen(),
        '/signup': (context) => SignupScreen(),
        '/chat': (context) => ChatScreen(),
      },
      initialRoute: '/login',
    );
  }
}
