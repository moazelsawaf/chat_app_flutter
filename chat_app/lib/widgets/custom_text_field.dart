import 'package:flutter/material.dart';
import '../util/constants.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final Function onSaved;
  final bool obsecure;

  CustomTextField({
    @required this.label,
    @required this.onSaved,
    this.obsecure = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: TextFormField(
        obscureText: obsecure,
        decoration: InputDecoration(
          hintText: label,
          fillColor: kWhite,
          filled: true,
          contentPadding: EdgeInsets.all(18),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide.none,
          ),
        ),
        validator: (value) =>
            value.isEmpty ? 'The ${label.toLowerCase()} is required!' : null,
        onSaved: onSaved,
      ),
    );
  }
}
