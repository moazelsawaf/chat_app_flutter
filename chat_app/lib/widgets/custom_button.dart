import 'package:flutter/material.dart';
import '../util/constants.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  CustomButton({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      height: 56,
      width: double.infinity,
      child: RaisedButton(
        padding: EdgeInsets.all(16),
        child: Text(
          text,
          style: TextStyle(color: kLightBrown, fontSize: 16),
        ),
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        onPressed: onPressed,
      ),
    );
  }
}
