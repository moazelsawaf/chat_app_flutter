import 'package:flutter/material.dart';
import 'package:flutter_day_project/util/constants.dart';

class MessageWidget extends StatelessWidget {
  final String message;
  final bool isMe;

  MessageWidget({this.message, this.isMe});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isMe ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.all(16),
        margin: isMe
            ? EdgeInsets.only(top: 8, bottom: 8, left: 16)
            : EdgeInsets.only(top: 8, bottom: 8, right: 16),
        decoration: BoxDecoration(
          color: isMe ? kYellow : kLightBrown,
          borderRadius: isMe
              ? BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                )
              : BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
        ),
        child: Text(
          message,
          style: TextStyle(color: kBrown, fontSize: 16),
        ),
      ),
    );
  }
}
