import 'package:flutter/material.dart';

const Color kYellow = Color(0xFFE8C23D);
const Color kBrown = Color(0xFF472A22);
const Color kLightBrown = Color(0xFFDDCEB6);
const Color kWhite = Color(0xFFFFFFFF);
