import 'package:flutter/material.dart';
import 'package:flutter_day_project/util/constants.dart';
import 'package:flutter_day_project/widgets/custom_text_field.dart';
import 'package:flutter_day_project/widgets/custom_button.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String _email;
  String _password;
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kYellow,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 48,
              ),
              Text(
                'Welcome!',
                style: TextStyle(color: kBrown, fontSize: 32),
              ),
              SizedBox(height: 24),
              Hero(
                tag: 'logo',
                child: Image.asset(
                  'assets/images/chat_image.png',
                  height: 170,
                ),
              ),
              SizedBox(height: 48),
              Form(
                key: _key,
                child: Column(
                  children: <Widget>[
                    CustomTextField(
                      label: 'Email',
                      onSaved: (value) => _email = value,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomTextField(
                      label: 'Password',
                      onSaved: (value) => _password = value,
                      obsecure: true,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomButton(
                      text: 'LOGIN',
                      onPressed: () {
                        if (_key.currentState.validate()) {
                          _key.currentState.save();
                          print('Email: $_email');
                          print('Password: $_password');

                          // TODO: Add ( login ) logic here

                          Navigator.pushReplacementNamed(context, '/chat');
                        }
                      },
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'New user? ',
                          style: TextStyle(color: kBrown, fontSize: 18),
                        ),
                        GestureDetector(
                          child: Text(
                            'Sign up',
                            style: TextStyle(
                                color: kBrown,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                          onTap: () => Navigator.pushNamed(context, '/signup'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 48,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
