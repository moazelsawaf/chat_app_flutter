import 'package:flutter/material.dart';
import 'package:flutter_day_project/util/constants.dart';
import 'package:flutter_day_project/widgets/custom_text_field.dart';
import 'package:flutter_day_project/widgets/custom_button.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  String _email;
  String _password;
  String _confirmPassword;
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kYellow,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 48,
              ),
              Text(
                'Create an account',
                style: TextStyle(color: kBrown, fontSize: 32),
              ),
              SizedBox(height: 24),
              Hero(
                tag: 'logo',
                child: Image.asset(
                  'assets/images/chat_image.png',
                  height: 170,
                ),
              ),
              SizedBox(height: 48),
              Form(
                key: _key,
                child: Column(
                  children: <Widget>[
                    CustomTextField(
                      label: 'Email',
                      onSaved: (value) => _email = value,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomTextField(
                      label: 'Password',
                      onSaved: (value) => _password = value,
                      obsecure: true,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomTextField(
                      label: 'Confirm Password',
                      onSaved: (value) => _confirmPassword = value,
                      obsecure: true,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomButton(
                      text: 'CREATE AN ACCOUNT',
                      onPressed: () {
                        if (_key.currentState.validate()) {
                          _key.currentState.save();
                          print('Email: $_email');
                          print('Password: $_password');
                          print('Confirm Password: $_confirmPassword');

                          // TODO: Add ( signup ) logic here

                          Navigator.pushReplacementNamed(context, '/chat');
                        }
                      },
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Do you have an account? ',
                          style: TextStyle(color: kBrown, fontSize: 18),
                        ),
                        GestureDetector(
                          child: Text(
                            'Login',
                            style: TextStyle(
                                color: kBrown,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                          onTap: () => Navigator.pushNamed(context, '/login'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 48,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
