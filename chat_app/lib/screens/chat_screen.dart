import 'package:flutter/material.dart';
import 'package:flutter_day_project/models/message.dart';
import 'package:flutter_day_project/util/constants.dart';
import 'package:flutter_day_project/widgets/message_widget.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController _controller = TextEditingController();

  List<Message> _messages = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      appBar: AppBar(
        title: Hero(
          tag: 'logo',
          child: Image.asset(
            'assets/images/chat_image.png',
            height: 35,
          ),
        ),
        centerTitle: true,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.exit_to_app,
            color: Colors.white,
          ),
          onPressed: () {
            // TODO: Add ( signout ) logic here

            Navigator.pushReplacementNamed(context, '/login');
          },
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              reverse: true,
              itemCount: _messages.length,
              itemBuilder: (context, index) => MessageWidget(
                message: _messages.reversed.elementAt(index).messageText,
                isMe: _messages.reversed.elementAt(index).isMe,
              ),
            ),
          ),
          Divider(
            color: kBrown,
            height: 0,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(16),
                    hintText: 'Type a message',
                    hintStyle: TextStyle(
                      fontSize: 16,
                      color: kBrown,
                    ),
                    border: InputBorder.none,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.send,
                  color: kBrown,
                ),
                onPressed: () {
                  final String message = _controller.text;

                  if (message.isEmpty) return;

                  setState(() {
                    // TODO: Add the ( chating ) login here

                    _messages.add(Message(messageText: message, isMe: true));
                  });

                  _controller.clear();
                },
              ),
            ],
          )
        ],
      ),
    );
  }
}
