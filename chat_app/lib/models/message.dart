class Message {
  String messageText;
  bool isMe;

  Message({this.messageText, this.isMe});
}
